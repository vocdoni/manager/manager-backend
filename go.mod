module gitlab.com/vocdoni/manager/manager-backend

go 1.14

require (
	firebase.google.com/go/v4 v4.0.0
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/badoux/checkmail v0.0.0-20181210160741-9661bd69e9ad
	github.com/ethereum/go-ethereum v1.9.20
	github.com/frankban/quicktest v1.11.3
	github.com/gobuffalo/packr/v2 v2.7.1
	github.com/google/uuid v1.1.1
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgtype v1.3.1-0.20200521144610-9d847241cb8f
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/jmoiron/sqlx v1.2.1-0.20200615141059-0794cb1f47ee
	github.com/knadh/smtppool v0.3.0
	github.com/lib/pq v1.8.0
	github.com/prometheus/client_golang v1.7.1
	github.com/rubenv/sql-migrate v0.0.0-20200616145509-8d140a17f351
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.0
	gitlab.com/vocdoni/go-dvote v0.6.1-0.20201109132313-5ef75c891237
	golang.org/x/mod v0.3.0 // indirect
	golang.org/x/tools v0.0.0-20200617042924-7f3f4b10a808 // indirect
	google.golang.org/api v0.17.0
	nhooyr.io/websocket v1.8.6
)
